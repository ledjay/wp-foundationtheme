<?php

function cfm_add_scripts() {
	// Register the script
	wp_register_script( 'jq', get_bloginfo("stylesheet_directory") . "/bower_components/jquery/dist/jquery.js", false, false, false );
	wp_register_script( 'what-input', get_bloginfo("stylesheet_directory") . "/bower_components/what-input/what-input.js", false, false, true );
	wp_register_script( 'foundation', get_bloginfo("stylesheet_directory") . "/bower_components/foundation-sites/dist/foundation.js", false, false, true );
	wp_register_script( 'app', get_bloginfo("stylesheet_directory") . "/js/app.js", false, false, true );
	 
	 
	// Enqueued script with localized data.
	if(!is_admin()) {
	 
	    wp_enqueue_script( 'jq' );
	    
	    wp_enqueue_script( 'what-input' );
	    wp_enqueue_script( 'jq.preload' );
	    wp_enqueue_script( 'foundation' );     
	    wp_enqueue_script( 'app' );
	}
} 

add_action( 'wp_footer', 'cfm_add_scripts' ); 