<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title><?php bloginfo("title"); ?></title>
	<link rel="stylesheet" href="<?php bloginfo("stylesheet_directory"); ?>/css/app.css">
	<?php wp_head(); ?>
</head>
<body>

<header>
	<div class="row">
		<div class="large-12 columns">
			<h1><?php bloginfo("title"); ?></h1>
		</div>
	</div>
</header>